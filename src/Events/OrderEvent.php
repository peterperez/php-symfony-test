<?php

namespace App\Events;

use App\Model\LogData;
use Symfony\Contracts\EventDispatcher\Event;

class OrderEvent extends Event
{

    public const ORDER_STATUS = 'order_status';

    protected $logData;

    public function __construct(LogData $logData)
    {
        $this->logData = $logData;
    }

    public function getLogData()
    {
        return $this->logData;
    }


}