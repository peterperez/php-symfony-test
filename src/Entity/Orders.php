<?php

namespace App\Entity;

use App\Repository\OrdersRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=OrdersRepository::class)
 */
class Orders
{

    public function __construct()
    {
        $this->orderItems = new ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank
     * @Assert\Positive
     * @ORM\Column(type="float")
     */
    private $total;

    /**
     * @Assert\NotBlank
     * @Assert\PositiveOrZero
     * @ORM\Column(type="float")
     */
    private $discount;

    /**
     * @Assert\NotBlank
     * @Assert\Length(
     *  min = 5,
     *  maxMessage = "Your shipping address must have {{ 5 }} characters and above"
     * )
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $shipping_details;

    /**
     * @Assert\Choice(
     *     choices = {"ORDER_RECEIVED", "ORDER_CANCELED", "ORDER_PROCESSING", "ORDER_READY_TO_SHIP", "ORDER_SHIPPED"},
     *     message = "Choose a valid status."
     * )
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @Groups("order_details")
     * @ORM\OneToMany(targetEntity="App\Entity\OrderItem", mappedBy="order")
     */
    private $orderItems;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    public function setDiscount(float $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function getShippingDetails(): ?string
    {
        return $this->shipping_details;
    }

    public function setShippingDetails(?string $shipping_details): self
    {
        $this->shipping_details = $shipping_details;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|OrderItem[]
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }
}
