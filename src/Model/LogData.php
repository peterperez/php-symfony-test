<?php

namespace App\Model;

class LogData
{
    private $order_id;
    private $issue_id;
    private $Status;
    private $staff_id;
    private $staff_username;
    private $issue_type;
    private $log_type;
    private $issue_reason;
    private $box_id;
    private $shipping_company;
    private $tracking_number;
    private $proof;

    public function getOrderId(): ?int
    {
        return $this->order_id;
    }

    public function setOrderId(int $order_id): self
    {
        $this->order_id = $order_id;

        return $this;
    }

    public function getIssueId(): ?int
    {
        return $this->issue_id;
    }

    public function setIssueId(int $issue_id): self
    {
        $this->issue_Id = $issue_id;
        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;
        return $this;
    }

    public function getLogType(): ?string
    {
        return $this->log_type;
    }

    public function setLogType(string $log_type): self
    {
        $this->log_type = $log_type;
        return $this;
    }

    public function getStaffId(): ?int
    {
        return $this->staff_id;
    }

    public function setStaffId(int $staff_id): self
    {
        $this->staff_id = $staff_id;
        return $this;
    }

    public function getStaffUsername(): ?string
    {
        return $this->staff_username;
    }

    public function setStaffUsername(string $staff_username): self
    {
        $this->staff_username = $staff_username;
        return $this;
    }

    public function getIssueType(): ?string
    {
        return $this->issue_type;
    }

    public function setIssueType(string $issue_type): self
    {
        $this->issue_type = $issue_type;
        return $this;
    }

    public function getIssueReason(): ?string
    {
        return $this->issue_reason;
    }

    public function setIssueReason(string $issue_reason): self
    {
        $this->issue_reason = $issue_reason;
        return $this;
    }

    public function getBoxId(): ?string
    {
        return $this->box_id;
    }

    public function setBoxId(string $box_id): self
    {
        $this->box_id = $box_id;
        return $this;
    }

    public function getShippingCompany(): ?string
    {
        return $this->shipping_company;
    }

    public function setShippingCompany(string $shipping_company): self
    {
        $this->shipping_company = $shipping_company;
        return $this;
    }

    public function getTrackingNumber(): ?string
    {
        return $this->tracking_number;
    }

    public function setTrackingNumber(string $tracking_number): self
    {
        $this->tracking_number = $tracking_number;
        return $this;
    }

    public function getProof(): ?string
    {
        return $this->proof;
    }

    public function setProof(string $proof): self
    {
        $this->proof = $proof;
        return $this;
    }
}
