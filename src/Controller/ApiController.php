<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Orders;
use App\Entity\Items;
use App\Entity\OrderItem;
use App\Entity\IssueTypes;
use App\Model\LogData;

use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Events\OrderEvent;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ApiController extends AbstractApiController
{
    
    public function allItems(): Response
    {
        $items = $this->getDoctrine()->getRepository(Items::class)->findAll();
        return $this->json([
            'status' => 200,
            'message' => 'All items.',
            'data' => $items
        ]);
    }

    public function createItem(Request $request, ValidatorInterface $validator): Response
    {
        $requestData = $request->toArray();

        $eManager = $this->getDoctrine()->getManager();
        
        $item = new Items();
        $item->setTitle($requestData['title']);
        $item->setDescription($requestData['description']);
        $item->setPrice($requestData['price']);
        $item->setCreatedAt(new \DateTime("now", new \DateTimeZone("UTC")));
        $item->setUpdatedAt(new \DateTime("now", new \DateTimeZone("UTC")));

        $errors = $validator->validate($item);

        if (count($errors) > 0) {
            return $this->json([
                'status' => 400,
                'message' => 'error',
                'error' => $errors
            ]);
        }

        $eManager->persist($item);
        $eManager->flush();

        return $this->json([
            'status' => 200,
            'message' => 'item created successfully.',
            'data' => $item
        ]);
    }

    public function allIssueTypes(SerializerInterface $serializer): Response
    {
        $issueTypes = $this->getDoctrine()->getRepository(IssueTypes::class)->findAll();

        $encoder = new JsonEncoder();
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            },
        ];
        $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);
        $serializer = new Serializer([$normalizer], [$encoder]);

        $json = $serializer->serialize($issueTypes, 'json');

        return $this->json([
            'status' => 200,
            'message' => 'all issue types.',
            'data' => json_decode($json, true)
        ]);
    }

    public function createIssueType(Request $request, ValidatorInterface $validator): Response
    {
        $requestData = $request->toArray();

        $eManager = $this->getDoctrine()->getManager();
        
        $issueType = new IssueTypes();
        $issueType->setType($requestData['type']);
        $issueType->setCreatedAt(new \DateTime("now", new \DateTimeZone("UTC")));
        $issueType->setUpdatedAt(new \DateTime("now", new \DateTimeZone("UTC")));

        $errors = $validator->validate($issueType);

        if (count($errors) > 0) {
            return $this->json([
                'status' => 400,
                'message' => 'error',
                'error' => $errors
            ]);
        }

        $eManager->persist($issueType);
        $eManager->flush();

        return $this->json([
            'status' => 200,
            'message' => 'issue created successfully.',
            'data' => $issueType
        ]);
    }

    public function allOrders(Request $request, SerializerInterface $serializer)
    {
        $orders = $this->getDoctrine()->getRepository(Orders::class)->findAll();

        $encoder = new JsonEncoder();
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            },
        ];
        $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);
        $serializer = new Serializer([$normalizer], [$encoder]);

        $json = $serializer->serialize($orders, 'json');

        return $this->json([
            'status' => 200,
            'message' => 'all orders.',
            'data' => json_decode($json, true)
        ]);

    }

    public function createOrder(Request $request, ValidatorInterface $validator, EventDispatcherinterface $eventDispatcher): Response
    {
        $requestData = $request->toArray();

        $eManager = $this->getDoctrine()->getManager();

        // // insert into orders table first
        $order = new Orders();
        $order->setTotal($requestData['total']);
        $order->setDiscount($requestData['discount']);
        $order->setShippingDetails($requestData['shipping_details']);
        $order->setStatus('ORDER_RECEIVED');
        $order->setCreatedAt(new \DateTime("now", new \DateTimeZone("UTC")));
        $order->setUpdatedAt(new \DateTime("now", new \DateTimeZone("UTC")));

        // validate order data
        $errors = $validator->validate($order);
        if (count($errors) > 0) {
            return $this->json([
                'status' => 400,
                'message' => 'error',
                'error' => $errors
            ]);
        }

        $eManager->persist($order);
        $eManager->flush();


        // now insert into order_item table
        for ($i = 0; $i < sizeof($requestData['items']); ++$i) {
            $item = $eManager->getRepository(Items::class)->find($requestData['items'][$i]['item_id']);

            // dd($item);

            $orderItem = new OrderItem;
            $orderItem->setOrder($order);
            $orderItem->setItem($item);
            $orderItem->setQuantity($requestData['items'][$i]['quantity']);
            $orderItem->setCreatedAt(new \DateTime("now", new \DateTimeZone("UTC")));
            $orderItem->setUpdatedAt(new \DateTime("now", new \DateTimeZone("UTC")));
            $eManager->persist($orderItem);
            $eManager->flush();
        }

        $eManager->flush();

        // dispatch event
        $logData = new LogData();
        $logData->setOrderId($order->getId());
        $logData->setStatus('ORDER_RECEIVED');
        $logData->setLogType('new');
        $orderEvent = new OrderEvent($logData);
        $eventDispatcher->dispatch($orderEvent, OrderEvent::ORDER_STATUS);

        return $this->json([
            'status' => 200,
            'message' => 'order created successfully.'
            // 'data' => $order
        ]);
    }

    public function cancelOrder(Request $request, EventDispatcherinterface $eventDispatcher): Response
    {
        $requestData = $request->toArray();
        $orderId = $requestData['id'];

        $eManager = $this->getDoctrine()->getManager();
        $order = $eManager->getRepository(Orders::class)->find($orderId);

        if(!$order) {
            throw $this->createNotFoundException(
                'No order found for id: '.$orderId
            );
        }

        $order->setStatus('ORDER_CANCELED');
        $order->setUpdatedAt(new \DateTime("now", new \DateTimeZone("UTC")));
        $eManager->flush();

        // dispatch event
        $logData = new LogData();
        $logData->setOrderId($order->getId());
        $logData->setStatus('ORDER_CANCELED');
        $logData->setLogType('canceled');
        $orderEvent = new OrderEvent($logData);
        $eventDispatcher->dispatch($orderEvent, OrderEvent::ORDER_STATUS);

        return $this->json([
            'status' => 200,
            'message' => 'order cacelled successfully.',
        ]);
    }

    
}
