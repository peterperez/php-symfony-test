<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\FileUploader;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Events\OrderEvent;

use Knp\Component\Pager\PaginatorInterface;

use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

use App\Entity\Orders;
use App\Entity\Package;
use App\Entity\Issues;
use App\Entity\IssueTypes;

use App\Model\LogData;

class ShippingController extends AbstractController
{
    
    
    public function index(Request $request, PaginatorInterface $paginator, EventDispatcherinterface $eventDispatcher): Response
    {
        $ordersRepo = $this->getDoctrine()->getRepository(Orders::class)->findBy([
            'status' => 'ORDER_READY_TO_SHIP'
        ]);
        $orders = $paginator->paginate(
            $ordersRepo,
            $request->query->getInt('page', 1),
            5
        );

        // $orders = $this->getDoctrine()->getRepository(Order::class)->findAll();
        
        return $this->render('users/shipper/index.html.twig', [
            'orders' => $orders
        ]);
    }

    public function viewOrder(Request $request): Response
    {
        $orderId = $request->get('id');

        $eManager = $this->getDoctrine()->getManager();
        $order = $eManager->getRepository(Orders::class)->find($orderId);

        $package = $eManager->getRepository(Package::class)->findOneBy([
            'orderId' => $orderId
        ]);

        // $issues = $eManager->getRepository(Issues::class)->findBy([
        //     'order_id' => $orderId
        // ]);

        $issues = $eManager->getRepository(Issues::class)->findByIdJoinedToIssueType($orderId);

        // dd($issues); 

        $issueTypes = $eManager->getRepository(IssueTypes::class)->findAll();

        return $this->render('users/shipper/order_details.html.twig', [
            'order' => $order,
            'package' => $package,
            'issues' => $issues,
            'issueTypes' => $issueTypes
        ]);
    }

    public function submitIssue(Request $request, EventDispatcherinterface $eventDispatcher): Response
    {
        $orderId = $request->get('id');

        $eManager = $this->getDoctrine()->getManager();
        $order = $eManager->getRepository(Orders::class)->find($orderId);

        if(!$order) {
            throw $this->createNotFoundException(
                'No order found for id: '.$orderId
            );
        }

        $order->setStatus('ORDER_PROCESSING');
        $order->setUpdatedAt(new \DateTime("now", new \DateTimeZone("UTC")));

        // find issueType
        $issueType = $eManager->getRepository(IssueTypes::class)->find($request->get('issueType'));

        // dd($issueType);

        // create new issue and persist to db
        $issue = new Issues();
        $issue->setOrderId($orderId);
        $issue->setType($issueType);
        $issue->setReason($request->get('reason'));
        $issue->setCreatedAt(new \DateTime("now", new \DateTimeZone("UTC")));
        $issue->setUpdatedAt(new \DateTime("now", new \DateTimeZone("UTC")));

        $eManager->persist($issue);

        $eManager->flush();

        // dispatch event
        $logData = new LogData();
        $logData->setOrderId($order->getId());
        $logData->setStatus('ORDER_PROCESSING');
        $logData->setLogType('reprocessing');
        $logData->setStaffId($this->getuser()->getId());
        $logData->setStaffUsername($this->getuser()->getUsername());
        $logData->setIssueType($issueType->getType());
        $logData->setIssueReason($request->get('reason'));

        $orderEvent = new OrderEvent($logData);
        $eventDispatcher->dispatch($orderEvent, OrderEvent::ORDER_STATUS);

        $package = $eManager->getRepository(Package::class)->findOneBy([
            'orderId' => $orderId
        ]);

        // $issues = $eManager->getRepository(Issues::class)->findBy([
        //     'order_id' => $orderId
        // ]);

        $issues = $eManager->getRepository(Issues::class)->findByIdJoinedToIssueType($orderId);

        $issueTypes = $eManager->getRepository(IssueTypes::class)->findAll();

        // dd($issues);

        return $this->render('users/shipper/order_details.html.twig', [
            'order' => $order,
            'package' => $package,
            'issues' => $issues,
            'issueTypes' => $issueTypes
        ]);
    }

    public function shipOrder(Request $request, FileUploader $fileUploader, EventDispatcherinterface $eventDispatcher): Response
    {
        $orderId = $request->get('id');

        $eManager = $this->getDoctrine()->getManager();
        $order = $eManager->getRepository(Orders::class)->find($orderId);

        if(!$order) {
            throw $this->createNotFoundException(
                'No order found for id: '.$orderId
            );
        }

        $order->setStatus('ORDER_SHIPPED');
        $order->setUpdatedAt(new \DateTime("now", new \DateTimeZone("UTC")));

        $oldPackage = $eManager->getRepository(Package::class)->findOneBy([
            'orderId' => $orderId
        ]);

        // update package
        $oldPackage->setShippingCompany($request->get('shippingCompany'));

        $file = $request->files->get('imageFile');

        $oldPackage->setImageFile($file);
        $oldPackage->setTrackingNumber($request->get('trackingNumber'));
        $oldPackage->setShippingUserId($this->getUser()->getId());
        $oldPackage->setUpdatedAt(new \DateTime("now", new \DateTimeZone("UTC")));

        $eManager->persist($oldPackage);
        $eManager->flush();

        //DISPATCH EVENT
        $logData = new LogData();
        $logData->setOrderId($order->getId());
        $logData->setStatus('ORDER_SHIPPED');
        $logData->setLogType('shipped');
        $logData->setStaffId($this->getuser()->getId());
        $logData->setStaffUsername($this->getuser()->getUsername());
        $logData->setShippingCompany($request->get('shippingCompany'));
        $logData->setTrackingNumber($request->get('trackingNumber'));
        $logData->setProof($oldPackage->getProof());

        $orderEvent = new OrderEvent($logData);
        $eventDispatcher->dispatch($orderEvent, OrderEvent::ORDER_STATUS);

        $package = $eManager->getRepository(Package::class)->findOneBy([
            'orderId' => $orderId
        ]);

        $issues = $eManager->getRepository(Issues::class)->findByIdJoinedToIssueType($orderId);
        $issueTypes = $eManager->getRepository(IssueTypes::class)->findAll();

        return $this->render('users/shipper/order_details.html.twig', [
            'order' => $order,
            'package' => $package,
            'issues' => $issues,
            'issueTypes' => $issueTypes
        ]);
    }

    /**
     * @Route("/download/{filename}", name="download_file")
    **/
    public function downloadFileAction(Request $request){
        $fileName = $request->get('filename');
        $response = new BinaryFileResponse($this->getParameter('proof_directory').'/'.$fileName);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        return $response;
    }
}
