<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Events\OrderEvent;

use Knp\Component\Pager\PaginatorInterface;

use App\Entity\Package;
use App\Entity\Issues;

use App\Entity\Orders;
use App\Entity\Items;
use App\Entity\OrderItem;
use App\Entity\IssueTypes;

use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

use App\Model\LogData;

class PickingController extends AbstractController
{
    
    
    public function index(Request $request, PaginatorInterface $paginator, EventDispatcherinterface $eventDispatcher): Response
    {

        $ordersRepo = $this->getDoctrine()->getRepository(Orders::class)->findBy([
            'status' => ['ORDER_RECEIVED', 'ORDER_PROCESSING'],
        ]);

        $orders = $paginator->paginate(
            $ordersRepo,
            $request->query->getInt('page', 1),
            5
        );

        return $this->render('users/picker/index.html.twig', [
            'orders' => $orders
        ]);
    }

    public function viewOrder(Request $request): Response
    {
        $orderId = $request->get('id');

        $eManager = $this->getDoctrine()->getManager();
        $order = $eManager->getRepository(Orders::class)->find($orderId);

        $package = $eManager->getRepository(Package::class)->findOneBy([
            'orderId' => $orderId
        ]);

        $issues = $eManager->getRepository(Issues::class)->findByIdJoinedToIssueType($orderId);

        return $this->render('users/picker/order_details.html.twig', [
            'order' => $order,
            'package' => $package,
            'issues' => $issues
        ]);
    }

    public function processingOrder(Request $request, EventDispatcherinterface $eventDispatcher): Response
    {
        $orderId = $request->get('id');

        $eManager = $this->getDoctrine()->getManager();
        $order = $eManager->getRepository(Orders::class)->find($orderId);

        if(!$order) {
            throw $this->createNotFoundException(
                'No order found for id: '.$orderId
            );
        }

        // check if there exist another order that is being processed now

        $order->setStatus('ORDER_PROCESSING');
        $order->setUpdatedAt(new \DateTime("now", new \DateTimeZone("UTC")));
        $eManager->flush();

        //DISPATCH EVENT
        $logData = new LogData();
        $logData->setOrderId($order->getId());
        $logData->setStatus('ORDER_PROCESSING');
        $logData->setLogType('processing');
        $logData->setStaffId($this->getuser()->getId());
        $logData->setStaffUsername($this->getuser()->getUsername());

        $orderEvent = new OrderEvent($logData);
        $eventDispatcher->dispatch($orderEvent, OrderEvent::ORDER_STATUS);

        $package = $eManager->getRepository(Package::class)->findOneBy([
            'orderId' => $orderId
        ]);

        $issues = $eManager->getRepository(Issues::class)->findByIdJoinedToIssueType($orderId);

        return $this->render('users/picker/order_details.html.twig', [
            'order' => $order,
            'package' => $package,
            'issues' => $issues,
            'error_message' => 'You can only process 1 order at a time.'
        ]);

    }

    public function orderProcessingComplete(Request $request, EventDispatcherinterface $eventDispatcher): Response
    {

        $boxId = $request->get('boxId');

        $orderId = $request->get('id');

        $eManager = $this->getDoctrine()->getManager();
        $order = $eManager->getRepository(Orders::class)->find($orderId);

        // dd($order);

        if(!$order) {
            throw $this->createNotFoundException(
                'No order found for id: '.$orderId
            );
        }

        // update status and timestamp
        $order->setStatus('ORDER_READY_TO_SHIP');
        $order->setUpdatedAt(new \DateTime("now", new \DateTimeZone("UTC")));
        // $eManager->persist($order);
        $eManager->flush();

        // check if package already exist
        // if a package already exists with the same orderId, proceed to update, else; create new record

        $oldPackage = $eManager->getRepository(Package::class)->findOneBy([
            'orderId' => $orderId
        ]);

        if($oldPackage) {
            // update old package
            $oldPackage->setBoxId($boxId);
            $oldPackage->setUpdatedAt(new \DateTime("now", new \DateTimeZone("UTC")));

            $eManager->persist($oldPackage);
        } else {
            // create new package and persist to db
            $package = new Package();
            $package->setOrderId($orderId);
            $package->setBoxId($boxId);
            $package->setPackingUserId($this->getUser()->getId());
            $package->setCreatedAt(new \DateTime("now", new \DateTimeZone("UTC")));
            $package->setUpdatedAt(new \DateTime("now", new \DateTimeZone("UTC")));

            $eManager->persist($package);
        }
        
        $eManager->flush();

        //DISPATCH EVENT
        $logData = new LogData();
        $logData->setOrderId($order->getId());
        $logData->setStatus('ORDER_READY_TO_SHIP');
        $logData->setLogType('ready');
        $logData->setStaffId($this->getuser()->getId());
        $logData->setStaffUsername($this->getuser()->getUsername());
        $logData->setBoxId($boxId);

        $orderEvent = new OrderEvent($logData);
        $eventDispatcher->dispatch($orderEvent, OrderEvent::ORDER_STATUS);

        $package = $eManager->getRepository(Package::class)->findOneBy([
            'orderId' => $orderId
        ]);

        $issues = $eManager->getRepository(Issues::class)->findByIdJoinedToIssueType($orderId);

        return $this->render('users/picker/order_details.html.twig', [
            'order' => $order,
            'package' => $package,
            'issues' => $issues
        ]);

    }
}
