<?php

namespace App\Repository;

use App\Entity\IssueTypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method IssueTypes|null find($id, $lockMode = null, $lockVersion = null)
 * @method IssueTypes|null findOneBy(array $criteria, array $orderBy = null)
 * @method IssueTypes[]    findAll()
 * @method IssueTypes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IssueTypesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IssueTypes::class);
    }

    // /**
    //  * @return IssueTypes[] Returns an array of IssueTypes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IssueTypes
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
